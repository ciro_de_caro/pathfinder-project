interface Ability {
  action_cost: string | null
  critical_failure: string | null
  critical_success: string | null
  description: string
  effect: string | null
  effects: string | null
  frequency: string | null
  full_description: string | null
  generic_description: string | null
  name: string
  range: string | null
  raw_description: string | null
  requirements: string | null
  success: string | null
  traits: string[]
  trigger: string | null
}