interface AbilityModifier {
  charisma: AbilityModifier
  constitution: AbilityModifier
  dexterity: AbilityModifier
  intelligence: AbilityModifier
  stength: AbilityModifier
  wisdom: AbilityModifier
}