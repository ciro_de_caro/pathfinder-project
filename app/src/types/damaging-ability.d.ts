interface DamagingAbility extends Ability {
  damage: Damage
  plus_damage: number | null
  to_hit: number
}