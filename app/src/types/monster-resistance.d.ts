interface MonsterResistance {
  amount: number
  type: string
}