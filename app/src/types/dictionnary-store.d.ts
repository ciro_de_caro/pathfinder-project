interface DictionnaryStore<K> {
  [key: string]: K
}