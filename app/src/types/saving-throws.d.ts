interface SavingThrows {
  fort: number
  fort_misc: number | null
  misc: number | null
  ref: number
  ref_misc: number | null
  will: number
  will_misc: number | null
}