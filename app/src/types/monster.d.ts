interface Monster {
  ability_mods: AbilityModifiers
  name: string
  ac: number
  ac_special: number
  alignement: string
  automatic_abilities: Ability[]
  description: string
  hp: number
  hp_misc: number | null
  immunities: string | null
  items: any[]
  languages: string[]
  level: number
  melee: DamagingAbility[]
  perception: number
  proactive_abilities: Ability[]
  ranged: DamagingAbility[]
  rarity: string
  resistances: MonsterResistance[]
  ritual_lists: any[]
  saves: SavingThrows
  sense_abilities: string | null
  senses: string[]
  size: string
  skills: MonsterSkill[]
  source: any[]
  speed: Speed[]
  spell_lists: any[]
  traits: string[]
  weaknesses: MonsterResistance[]
}
