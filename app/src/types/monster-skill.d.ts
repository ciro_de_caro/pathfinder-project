interface MonsterSkill {
  bonus: number
  misc: string | null
  name: string
}