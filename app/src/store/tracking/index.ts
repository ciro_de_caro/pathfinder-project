import combat from '@/store/tracking/combat'

export default {
  namespaced: true,
  state: (): any => ({}),
  mutations: {
  },
  actions: {
  },
  getters: {},
  modules: {
    combat
  }
}