import { v4 as uuidv4 } from 'uuid';
import { ActionContext } from 'vuex'
import { invoke } from '@tauri-apps/api/tauri';

export interface State {
  data: DictionnaryStore<CombatTrackingSelection[]>,
  selectedId: string | null
}

interface SetArgs {
  id: string
  data: CombatTrackingSelection[]
}

interface AddToCurrentSelectionArgs {
  data: CombatTrackingSelection
}

const storageKey = 'tracking/combat'

const addIdIfNoId = (data: DictionnaryStore<CombatTrackingSelection[]>): DictionnaryStore<CombatTrackingSelection[]> => {
  return Object.keys(data).reduce((acc: DictionnaryStore<CombatTrackingSelection[]>, key: string) => {
    acc[key] = data[key].map((a: CombatTrackingSelection) => {
      if (typeof a.id === 'undefined') a.id = uuidv4()
      return a
    })
    return acc
  }, {})
}

export default {
  namespaced: true,
  state: (): State => ({
    data: {},
    selectedId: null,
  }),
  mutations: {
    setData(state: State, data: DictionnaryStore<CombatTrackingSelection[]>): void {
      state.data = addIdIfNoId(data)
    },
    set(state: State, { id, data }: SetArgs): void {
      state.data[id] = addIdIfNoId({ [id]: data })[id]
    },
    setSelectedId(state: State, id: string): void {
      state.selectedId = id
    }
  },
  actions: {
    async fetch({ commit }: ActionContext<State, State>): Promise<void> {
      if (window.__TAURI__) {
        try {
          const data = await invoke('command_parser', {
            cmd: {
              command: 'fetchTrackersCombatData'
            },
          })
          commit('setData', data)
        } catch (e) {
          console.error(e)
        }
      } else {
        const data = localStorage.getItem(storageKey)
        if (data) {
          try {
            commit('setData', JSON.parse(data))
          } catch (e) {
            console.error('Malformed JSON in local storage', data)
          }
        }
      }
    },
    addToSelection({ commit, getters, dispatch }: ActionContext<State, State>, { data }: AddToCurrentSelectionArgs): void {
      const nData = getters.selection.push(data)
      commit('set', { id: getters.selectedId, data: nData })
      dispatch('save')
    },
    set({ commit, dispatch }: ActionContext<State, State>, { id, data }: SetArgs): void {
      commit('set', { id, data })
      dispatch('save')
    },
    async save({ getters }: ActionContext<State, State>): Promise<void> {
      if (window.__TAURI__) {
        try {
          await invoke('command_parser', {
            cmd: {
              command: 'setTrackersCombatData',
              key: "combat",
              data: getters.data
            },
          })
        } catch (e) {
          console.error(e)
        }
      } else {
        localStorage.setItem(storageKey, JSON.stringify(getters.data))
      }
    }
  },
  getters: {
    data: (state: State): DictionnaryStore<CombatTrackingSelection[]> => {
      return state.data
    },
    selection: (state: State): CombatTrackingSelection[] | null => {
      return state.selectedId !== null ? state.data[state.selectedId] : null
    },
    selectedId: (state: State): string | null => {
      return state.selectedId
    }
  },
  modules: {
  }
}