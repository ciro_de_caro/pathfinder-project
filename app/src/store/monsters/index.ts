import monstersData from './datas'

export interface State {
  data: Monster[]
}

export default {
  namespaced: true,
  state: (): any => ({
    data: monstersData
  }),
  mutations: {
  },
  actions: {
  },
  getters: {
    data: (state: State): Monster[] => {
      return state.data
    }
  },
  modules: {
  }
}