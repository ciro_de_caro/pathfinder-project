import { createStore } from 'vuex'

import monsters from '@/store/monsters'
import tracking from '@/store/tracking'

export default createStore({
  state: (): any => ({}),
  mutations: {
  },
  actions: {
  },
  modules: {
    monsters,
    tracking
  }
})
