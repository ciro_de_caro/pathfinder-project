interface RollArgs {
  diceFaces: number
}

export default ({ diceFaces }: RollArgs = { diceFaces: 20 }): number => {
  const dice = Math.ceil(Math.random() * (diceFaces - 1) + 1)
  return dice
}