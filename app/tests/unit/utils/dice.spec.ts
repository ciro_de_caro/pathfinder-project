import roll from '@/utils/dice'

describe('utils/dice', () => {
  describe.each(Array.from(Array(1000), () => new Array(0)))('roll', () => {
    it('should create a number between 1 and 4', () => {
        const r = roll({ diceFaces: 4 })
        expect(r <= 4 && r >= 1).toBeTruthy()
    })

    it('should create a number between 1 and 6', () => {
        const r = roll({ diceFaces: 6 })
        expect(r <= 6 && r >= 1).toBeTruthy()
    })

    it('should create a number between 1 and 10', () => {
        const r = roll({ diceFaces: 10 })
        expect(r <= 10 && r >= 1).toBeTruthy()
    })

    it('should create a number between 1 and 12', () => {
        const r = roll({ diceFaces: 12 })
        expect(r <= 12 && r >= 1).toBeTruthy()
    })

    it('should create a number between 1 and 20', () => {
        const r = roll()
        expect(r <= 20 && r >= 1).toBeTruthy()
    })

    it('should create a number between 1 and 100', () => {
        const r = roll({ diceFaces: 100 })
        expect(r <= 100 && r >= 1).toBeTruthy()
    })
  })
})