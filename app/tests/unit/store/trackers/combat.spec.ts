import { createStore, Store } from 'vuex'
import StoreInstance, { State } from '@/store/tracking/combat'

const mockData = {"default":[{"name":"test-id-1","id":"d06df68b-7474-40f7-ab33-c43ff2b98d46"},{"name":"test-id-2","id":"8d9e5300-bb90-4f78-a911-31cca803b6e4"},{"name":"test-id-3","id":"d44a8690-4c98-4c3f-8d4a-0d3910bfbbe5"}]}

describe('store/trackers/combat', () => {
  let store: Store<State>;
  beforeEach(() => {
    store = createStore(StoreInstance)
  })

  it('should have a default state to nothing', () => {
    expect(store.state.data).toEqual({})
    expect(store.state.selectedId).toBeNull()
  })

  describe('fetch', () => {
    it('should get default state if setted in localStorage', () => {
      localStorage.setItem('tracking/combat', JSON.stringify(mockData))
      store.dispatch('fetch')
      expect(store.state.data).toEqual(mockData)
      expect(store.state.selectedId).toBeNull()
    })
  })
})
