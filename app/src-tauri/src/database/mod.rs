use std::sync::Mutex;
use std::collections::HashMap;
use std::fs;
use std::io::Write;
use std::ops::Deref;
use serde::{ Serialize, Deserialize};

mod trackers;

const DATABASE_FILE_NAME: &'static str = "database.json"; 

#[derive(Debug)]
pub struct Storage(Mutex<Database>);

impl Storage {
  pub fn new() -> Storage {
    Storage(Mutex::new(Database::new()))
  }
}

impl Deref for Storage {
  type Target = Mutex<Database>; // Our wrapper struct will coerce into Storage
  fn deref(&self) -> &Mutex<Database> {
      &self.0 // We just extract the inner element
  }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Database {
  file_path: Option<std::path::PathBuf>,
  synced: bool,
  trackers: trackers::Trackers
}

impl Database {
  pub fn new() -> Database {
    Database {
      file_path: None,
      synced: false,
      trackers: trackers::Trackers::new()
    }
  }

  /// This function is to init the database folder and file.
  /// If the folder doesn't exists, it creates it and write the
  /// database file into it.
  pub fn init(&mut self, package_name: &str) -> Result<(), String> {
    if !self.synced {
      let root_path = tauri::api::path::data_dir().unwrap().join(package_name);
      if !root_path.is_dir() {
        self.file_path = Some(self.create(&root_path)?);
      } else {
        self.file_path = Some(self.load(&root_path)?);
      }
    }
    Ok(())
  }

  pub fn trackers(&self) -> &trackers::Trackers {
    &self.trackers
  }

  pub fn set(&mut self, tracker_name: &str, key: &str, data: serde_json::Value) -> Result<(), String> {
    match tracker_name {
      "trackers" => {
        self.trackers.set(key, data)?;
        self.save()
      },
      _ => Err(format!("no tracker named {}", tracker_name))
    }
  }

  fn as_bytes(&self) -> Vec<u8> {
    let mut export: HashMap<String, serde_json::Value> = HashMap::new();
    export.insert("combat".to_string(), serde_json::to_value(self.trackers().combat()).unwrap());
    serde_json::to_vec(&serde_json::to_value(&export).unwrap()).unwrap()
  }

  fn create(&mut self, root_path: &std::path::PathBuf) -> Result<std::path::PathBuf, String> {
    match fs::create_dir(root_path) {
      Ok(()) => {
        self.create_file(root_path)
      },
      Err(e) => self.create_file(root_path)
    }
  }

  fn create_file(&self, root_path: &std::path::PathBuf) -> Result<std::path::PathBuf, String> {
    let file_path = root_path.join(DATABASE_FILE_NAME);
    match fs::File::create(&file_path) {
      Ok(mut file) => {
        match file.write_all(&self.as_bytes()) {
          Ok(_) => {
            Ok(file_path)
          },
          Err(e) => Err(e.to_string())
        }
      },
      Err(e) => Err(e.to_string())
    }
  }

  fn load(&mut self, root_path: &std::path::PathBuf) -> Result<std::path::PathBuf, String> {
    let file_path = root_path.join(DATABASE_FILE_NAME);
    if !file_path.exists() {
      self.create(root_path)?;
    }
    match fs::read_to_string(&file_path) {
      Ok(s) => {
        match serde_json::from_str::<trackers::Trackers>(&s) {
          Ok(d) => {
            self.trackers = d;
            self.synced = true;
            Ok(file_path)
          },
          Err(e) => Err(e.to_string())
        }
      },
      Err(e) => Err(e.to_string())
    }
  }

  fn save(&self) -> Result<(), String> {
    match self.file_path {
      Some(ref file_path) => match std::fs::OpenOptions::new().write(true).truncate(true).open(file_path) {
        Ok(mut file) => match file.write_all(&self.as_bytes()) {
          Ok(_) => Ok(()),
          Err(e) => Err(format!("Database.save.write_all ERROR: {:?}", e))
        },
        Err(e) => Err(format!("Database.save.File::open ERROR: {:?}", e))
      },
      None => Err("no file initiated".to_string())
    }
  }
}