use std::collections::HashMap;
use serde::{ Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TrackerItem {
  id: String,
  name: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Trackers {
  combat: HashMap<String, Vec<TrackerItem>>
}

impl Trackers {
  pub fn new() -> Trackers {
    Trackers {
      combat: HashMap::new()
    }
  }

  pub fn combat(&self) -> &HashMap<String, Vec<TrackerItem>> {
    &self.combat
  }

  pub fn contains_key(&self, key: &str) -> bool {
    match key {
      "combat" => true,
      _ => false
    }
  }

  pub fn get(&self, key: &str) -> Result<&HashMap<String, Vec<TrackerItem>>, String> {
    match key {
      "combat" => Ok(self.combat()),
      _ => Err(format!("no key named {}", key))
    }
  }

  pub fn set(&mut self, key: &str, data: serde_json::Value) -> Result<(), String> {
    match key {
      "combat" => {
        match serde_json::from_value::<HashMap<String, Vec<TrackerItem>>>(data) {
          Ok(d) => {
            self.combat = d;
            Ok(())
          },
          Err(e) => Err(e.to_string())
        }
      },
      _ => Err(format!("no key named {}", key))
    }
  }
}