#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]
extern crate serde_json;

mod database;
mod command;

use database::Storage;

#[tauri::command]
async fn command_parser(
  window: tauri::Window,
  app_handle: tauri::AppHandle,
  cmd: command::Command,
  storage: tauri::State<'_, Storage>,
) -> Result<serde_json::Value, String> {
  println!("Called from {}", window.label());
  println!("received {:?}", cmd);
  cmd.invoke(app_handle, storage).await
}

fn main() {
  tauri::Builder::default()
    .manage(Storage::new())
    .invoke_handler(tauri::generate_handler![command_parser])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}
