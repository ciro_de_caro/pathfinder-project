use serde::Deserialize;
use crate::database::Storage;

#[derive(Deserialize, Clone, Debug)]
#[serde(tag = "command", rename_all = "camelCase")]
pub enum Command {
  // your custom commands
  // multiple arguments are allowed
  // note that rename_all = "camelCase": you need to use "myCustomCommand" on JS
  SetTrackersCombatData {
    data: serde_json::Value,
  },
  FetchTrackersCombatData
}

impl Command {
  pub async fn invoke(&self, app_handle: tauri::AppHandle, storage: tauri::State<'_, Storage>) -> Result<serde_json::Value, String> {
    match self {
      Command::FetchTrackersCombatData => {
        storage.lock().unwrap().init(&app_handle.package_info().name)?;
        Ok(serde_json::to_value(storage.lock().unwrap().trackers().combat()).unwrap())
      },
      Command::SetTrackersCombatData { data } => {
        storage.lock().unwrap().init(&app_handle.package_info().name)?;
        storage.lock().unwrap().set("trackers", "combat", data.clone())?;
        Ok(serde_json::to_value(storage.lock().unwrap().trackers().combat()).unwrap())
      },
      _ => Err("No result".into())
    }
  }
}