module.exports = {
  chainWebpack: config => {
    // yaml Loader
    config.module
      .rule('import-yaml')
      .test(/\.yaml$/)
      .type('json')
      .use('yaml-loader')
      .loader('yaml-loader')
      .end()
  }
}
