import marked from 'marked';
import { spell } from '../../../datas/spells.yaml';
import { condition } from '../../../datas/conditions.yaml';
import { feat } from '../../../datas/feats.yaml';
import { action } from '../../../datas/actions.yaml';
import { weapons } from '../../../datas/weapons.yaml';
import { armor } from '../../../datas/armor.yaml';
import { gear } from '../../../datas/gear.yaml';
import { background } from '../../../datas/backgrounds.yaml';
import { ammunition } from '../../../datas/ammunition.yaml';
import { ancestries } from '../../../datas/ancestriesheritages.yaml';
import { familiar_abilities as familiarAbilities } from '../../../datas/familiars.yaml';
import { language } from '../../../datas/langs.yaml';
import { damagecategory } from '../../../datas/damage.yaml';
import staves from '../../../datas/items-staves.yaml';

const getCast = function (item) {
  if (item.cast.includes('|1|')) {
    return `<div class="action-icon-container action-icon-container--one-item"><div class="action-icon-1"></div></div> ${item.cast.replace('|1| ', '')}`;
  } else if (item.cast.includes('|2|')) {
    return `<div class="action-icon-container action-icon-container--two-items"><div class="action-icon-1"></div><div class="action-icon-2"></div></div>  ${item.cast.replace('|2| ', '')}`;
  } else if (item.cast.includes('|3|')) {
    return `<div class="action-icon-container action-icon-container--three-items"><div class="action-icon-1"></div><div class="action-icon-2"></div><div class="action-icon-3"></div></div>  ${item.cast.replace('|3| ', '')}`;
  } else if (item.cast.includes('|R|')) {
    return `<div class="action-icon-container action-icon-container--one-item"><div class="action-icon-reaction"></div></div>  ${item.cast.replace('|R| ', '')}`;
  } else if (item.cast.includes('|F|')) {
    return `<div class="action-icon-container action-icon-container--one-item"><div class="action-icon-free"></div></div>  ${item.cast.replace('|F| ', '')}`;
  }
}

const getActionName = function (item) {
  if (item.actioncost_name === 'Single Action') {
    return `<div class="action-icon-container action-icon-container--one-item"><div class="action-icon-1"></div></div> ${item.actioncost_name}`;
  } else if (item.actioncost_name === 'Two Actions') {
    return `<div class="action-icon-container action-icon-container--two-items"><div class="action-icon-1"></div><div class="action-icon-2"></div></div>  ${item.actioncost_name}`;
  } else if (item.actioncost_name === 'Three Actions') {
    return `<div class="action-icon-container action-icon-container--three-items"><div class="action-icon-1"></div><div class="action-icon-2"></div><div class="action-icon-3"></div></div>  ${item.actioncost_name}`;
  } else if (item.actioncost_name === 'Free Action') {
    return `<div class="action-icon-container action-icon-container--one-item"><div class="action-icon-free"></div></div>  ${item.actioncost_name}`;
  } else {
    return item.actioncost_name;
  }
}

const getRealNameOfSource = function (abbr) {
  if (abbr === 'CRB') return 'Core Rule Book'
  else return abbr
}

const getSource = function (item) {
  if (!item.source) return 'No Source ?'
  return item.source.reduce((acc, currentValue, index) => {
    return index > 0 ? `${acc}<br>${getRealNameOfSource(currentValue.abbr)} p.${currentValue.page_start} - ${currentValue.page_stop}`
      : `${getRealNameOfSource(currentValue.abbr)} p.${currentValue.page_start} - ${currentValue.page_stop}`
  }, '')
}

const totalData = [
  ...spell.map(s => ({ ...s, vracCategory: 'spell' })),
  ...condition.map(s => ({ ...s, vracCategory: 'condition' })),
  ...feat.map(s => ({ ...s, vracCategory: 'feat' })),
  ...action.map(s => ({ ...s, vracCategory: 'action' })),
  ...weapons.map(s => ({ ...s, vracCategory: 'weapons' })),
  ...staves.map(s => ({ ...s, vracCategory: 'staves' })),
  ...armor.map(s => ({ ...s, vracCategory: 'armor' })),
  ...gear.map(s => ({ ...s, vracCategory: 'gear' })),
  ...background.map(s => ({ ...s, vracCategory: 'background' })),
  ...ammunition.map(s => ({ ...s, vracCategory: 'ammunition' })),
  ...ancestries.map(s => ({ ...s, vracCategory: 'ancestries' })),
  ...familiarAbilities.map(s => ({ ...s, vracCategory: 'familiarAbilities' })),
  ...language.map(s => ({ ...s, vracCategory: 'language' })),
  ...damagecategory.map(s => ({ ...s, vracCategory: 'damagecategory' })),
].map((s, i) => ({
  ...s,
  descr: s.descr ? marked(s.descr) : s.descr,
  cast: s.cast ? getCast(s) : s.cast,
  actioncost_name: s.actioncost_name ? getActionName(s) : s.actioncost_name,
  source: getSource(s),
  list_index: i
}));

export default {
  computed: {
    vracSearch () {
      return totalData.filter(s => this.inputSearch.trim() === '' || s.name.toLowerCase().includes(this.inputSearch.toLowerCase()));
    },
    allCategories () {
      return totalData.reduce((acc, currentValue) => {
        if (!acc.includes(currentValue.vracCategory)) acc.push(currentValue.vracCategory)
        return acc;
      }, [])
    }
  },
  methods: {
    watchScroll (event) {
      const limit = (event.target.scrollHeight - event.target.offsetHeight) - 50;
      if (limit <= (event.target.scrollTop + event.target.offsetHeight)) {
        const newSliceLimit = this.sliceEnd + this.sliceChunk;
        const maxLimit = this.vracSearch.length - 1;
        this.sliceEnd = newSliceLimit <= maxLimit ? newSliceLimit : (maxLimit >= this.sliceChunk ? maxLimit : this.sliceChunk);
      }
    }
  },
  mounted () {
    this.sliceEnd = this.sliceChunk;
  }
}
