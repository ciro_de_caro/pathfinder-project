import Vue from 'vue'
import VueRouter from 'vue-router'
import VracView from '../views/VracView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'VracView',
    component: VracView
  },
  {
    path: '/feats',
    name: 'Feats',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "feats-view" */ '../views/FeatsView.vue')
  },
  {
    path: '/spells',
    name: 'Spells',
    component: () => import(/* webpackChunkName: "spells-view" */ '../views/SpellsView.vue')
  },
  {
    path: '/detail/:index',
    name: 'Detail',
    props: true,
    component: () => import(/* webpackChunkName: "detail-view" */ '../views/DetailView.vue')
  },
  {
    path: '/sheet',
    name: 'Caracter Sheet',
    component: () => import(/* webpackChunkName: "sheet-view" */ '../views/SheetView.vue')
  },
]

const router = new VueRouter({
  routes
})

export default router
