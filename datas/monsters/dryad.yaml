ability_mods:
  cha_mod: 4
  con_mod: 1
  dex_mod: 4
  int_mod: 2
  str_mod: 0
  wis_mod: 3
ac: 19
ac_special: null
alignment: CG
automatic_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: A dryad is mystically bonded to a single great tree and must remain
    within 300 feet of it. If she moves beyond that range, she becomes sickened 1
    and is unable to recover. She must attempt a DC 18 Fortitude save every hour or
    increase the sickened value by 1 (to a maximum of sickened 4). After 24 hours,
    she becomes drained 1, with this value increasing by 1 every additional 24 hours.
    A dryad can perform a 24-hour ritual to bond herself to a new tree.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Tree Dependent
  range: null
  raw_description: '**Tree Dependent** A dryad is mystically bonded to a single great
    tree and must remain within 300 feet of it. If she moves beyond that range, she
    becomes sickened 1 and is unable to recover. She must attempt a DC 18 Fortitude
    save every hour or increase the sickened value by 1 (to a maximum of sickened
    4). After 24 hours, she becomes drained 1, with this value increasing by 1 every
    additional 24 hours. A dryad can perform a 24-hour ritual to bond herself to a
    new tree.'
  requirements: null
  success: null
  traits: null
  trigger: null
description: 'Dryads are fey guardians of the trees and creatures who dwell in wooded
  areas. They prefer using indirect methods to dissuade those who would harm their
  sacred groves and beloved forests, but they are not above using enchantments to
  enlist the aid of allies when evil threats cannot be dissuaded with words alone.
  In times of peace, dryads happily live secluded lives inside their trees, and a
  community at harmony with nature might not even realize a dryad lives nearby.




  Though they watch over all the woods around them, dryads are inextricably tied to
  a specific tree, usually an oak. Dryads who are bonded to another type of tree are
  fundamentally the same, but they may differ in temperament and appearance to match
  their ward. For instance, kraneiai, or cherry-tree dryads, have beautiful pink coloration
  and concern themselves with the fragile beauty of life.




  **__Recall Knowledge - Fey__ (__Nature__)**: DC 18


  **__Recall Knowledge - Plant__ (__Nature__)**: DC 18'
hp: 55
hp_misc: null
immunities: null
items: null
languages:
- Common
- Elven
- Sylvan
- speak with plants
level: 3
melee:
- action_cost: One Action
  damage:
    formula: 1d12+2
    type: bludgeoning
  name: branch
  plus_damage: null
  to_hit: 12
  traits:
  - finesse
  - magical
name: Dryad
perception: 10
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: 'The dryad touches a tree of enough volume to contain her and merges
    into it for as long as she wishes. She can Cast a Spell while inside as long as
    the spell doesn''t require a line of effect outside the tree. She can hear, but
    not see, what''s going on outside the tree. She can Dismiss this effect.


    Significant physical damage dealt to the tree expels the dryad from the tree and
    deals 3d6 damage to her. __Passwall__ expels the dryad without dealing damage.


    If a dryad uses this ability on her bonded tree, she instead enters an extradimensional
    living space within the tree; Tree Meld gains the __extradimensional__ trait.
    A dryad can bring up to two other creatures with her when entering her home within
    her bonded tree. The dryad can still be expelled from this space as above.'
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Tree Meld
  range: null
  raw_description: '**Tree Meld** [Two Actions]  (__primal__, __transmutation__) The
    dryad touches a tree of enough volume to contain her and merges into it for as
    long as she wishes. She can Cast a Spell while inside as long as the spell doesn''t
    require a line of effect outside the tree. She can hear, but not see, what''s
    going on outside the tree. She can Dismiss this effect.


    Significant physical damage dealt to the tree expels the dryad from the tree and
    deals 3d6 damage to her. __Passwall__ expels the dryad without dealing damage.


    If a dryad uses this ability on her bonded tree, she instead enters an extradimensional
    living space within the tree; Tree Meld gains the __extradimensional__ trait.
    A dryad can bring up to two other creatures with her when entering her home within
    her bonded tree. The dryad can still be expelled from this space as above.'
  requirements: null
  success: null
  traits:
  - primal
  - transmutation
  trigger: null
ranged: null
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 6
  fort_misc: null
  misc: null
  ref: 11
  ref_misc: null
  will: 10
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dryad can use Diplomacy to Make an Impression on and make very
    simple Requests of animals and plants.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Nature Empathy
  range: null
  raw_description: '**Nature Empathy** The dryad can use Diplomacy to Make an Impression
    on and make very simple Requests of animals and plants.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +10
- low-light vision
size: Medium
skills:
- bonus: 9
  misc: null
  name: 'Acrobatics '
- bonus: 5
  misc: null
  name: 'Athletics '
- bonus: 7
  misc: +9 woodworking
  name: 'Crafting '
- bonus: 9
  misc: null
  name: 'Diplomacy '
- bonus: 13
  misc: null
  name: 'Nature '
- bonus: 9
  misc: null
  name: 'Stealth '
- bonus: 12
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary
  page_start: 246
  page_stop: null
speed:
- amount: 25
  type: Land
spell_lists:
- dc: 21
  misc: ''
  name: Primal Innate Spells
  spell_groups:
  - heightened_level: null
    level: 5
    spells:
    - frequency: x2
      name: tree stride
      requirement: null
  - heightened_level: null
    level: 4
    spells:
    - frequency: x3
      name: charm
      requirement: null
    - frequency: null
      name: suggestion
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: sleep
      requirement: null
  - heightened_level: null
    level: 2
    spells:
    - frequency: at will
      name: entangle
      requirement: null
    - frequency: at will
      name: tree shape
      requirement: null
  - heightened_level: 2
    level: 0
    spells:
    - frequency: null
      name: tanglefoot
      requirement: null
  - heightened_level: 4
    level: -1
    spells:
    - frequency: null
      name: speak with plants
      requirement: null
  to_hit: 11
traits:
- CG
- Medium
- Fey
- Nymph
- Plant
type: Creature
weaknesses:
- amount: 5
  type: cold iron
- amount: 5
  type: fire
