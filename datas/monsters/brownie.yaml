ability_mods:
  cha_mod: 3
  con_mod: 1
  dex_mod: 4
  int_mod: 2
  str_mod: -2
  wis_mod: 4
ac: 16
ac_special: null
alignment: N
automatic_abilities: null
description: 'Brownies make their homes in the trunks of hollow trees, small earthy
  burrows, and even under porches or within the crawl spaces of farmhouses. Often
  attired in clothes that appear to be made of plants or leaves, brownies wear belts
  lined with pouches and tools. Whatever language they choose to speak is often is
  riddled with odd pronunciations and colloquialisms. Their manner of speaking might
  call upon turns of phrase that are decades or even centuries out of vogue, for example,
  or they might mix up their metaphors in strange ways. It almost seems as if brownies
  adopt these quirky ways of speaking intentionally—certainly they do not react favorably
  to corrections to their chatter. There''s often no swifter way to annoy a brownie
  than to try to correct its grammar. Brownies stand barely 2 feet tall and weigh
  about 20 pounds.




  When facing danger, brownies rarely engage in combat, preferring instead to confound
  and confuse their attackers in order to buy enough time for escape. Content with
  honest toil and the love of their kin, brownies maintain a pacifist nature, harassing
  creatures only to run them off or punish them for an insult. Despite this nature,
  all brownies carry a blade. They refer to their swords with a hint of disgust, and
  jokingly call their blades their "final trick," reserving their use for the direst
  of circumstances.




  Honest to a fault, brownies take freely but always repay their debt through work
  or leave something behind as an offering. They may eat an apple from a farmer''s
  orchard but harvest the entire tree as repayment. A brownie might eat an entire
  pie left on a windowsill, only to straighten up the kitchen or wash the dishes.
  A brownie can share a home with a family for years and years while avoiding detection.
  A family that is aware of a brownie in their midst usually finds this a beneficial
  relationship and leaves dishes of milk, pieces of fruit, trinkets, and sometimes
  even wine as gifts. In exchange, the resident brownie keeps the home clean, mends
  clothes, repairs tools, and shoos away vermin and small predators. Bragging about
  having a brownie in the house is the best way to lose one. Brownies distrust foxes
  and fear __wolves__, and they avoid farms with dogs.




  **__Recall Knowledge - Fey__ (__Nature__)**: DC 15'
hp: 25
hp_misc: null
immunities: null
items:
- shortsword
languages:
- Common
- Elven
- Gnomish
- Sylvan
level: 1
melee:
- action_cost: One Action
  damage:
    formula: 1d6
    type: null
  name: shortsword
  plus_damage: null
  to_hit: 7
  traits:
  - agile
  - finesse
name: Brownie
perception: 7
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: "The brownie's antics can confuse and disorient a creature. When the\
    \ brownie uses Baffling Bluff, it targets a single creature within 30 feet; that\
    \ creature must attempt a DC 17 Will save. The target is temporarily immune to\
    \ Baffling Bluff for 1 minute. \n\n**Critical Success **The target is unaffected.\
    \ \n\n**Success **The target is fooled momentarily and is __flat-footed__ against\
    \ the next melee Strike the brownie makes against it before the end of the brownie's\
    \ next turn. \n\n**Failure **The target is __confused__ for 1 round. \n\n**Critical\
    \ Failure **The target is confused for 1 minute. It can attempt a new save at\
    \ the end of each of its turns to end the confused condition."
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Baffling Bluff
  range: null
  raw_description: "**Baffling Bluff** [Two Actions]  (__emotion__, __enchantment__,\
    \ __mental__, __primal__) The brownie's antics can confuse and disorient a creature.\
    \ When the brownie uses Baffling Bluff, it targets a single creature within 30\
    \ feet; that creature must attempt a DC 17 Will save. The target is temporarily\
    \ immune to Baffling Bluff for 1 minute. \n\n**Critical Success **The target is\
    \ unaffected. \n\n**Success **The target is fooled momentarily and is __flat-footed__\
    \ against the next melee Strike the brownie makes against it before the end of\
    \ the brownie's next turn. \n\n**Failure **The target is __confused__ for 1 round.\
    \ \n\n**Critical Failure **The target is confused for 1 minute. It can attempt\
    \ a new save at the end of each of its turns to end the confused condition."
  requirements: null
  success: null
  traits:
  - emotion
  - enchantment
  - mental
  - primal
  trigger: null
ranged: null
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 4
  fort_misc: null
  misc: null
  ref: 9
  ref_misc: null
  will: 9
  will_misc: null
sense_abilities: null
senses:
- Perception +7
- low-light vision
size: Tiny
skills:
- bonus: 7
  misc: null
  name: 'Acrobatics '
- bonus: 5
  misc: null
  name: 'Crafting '
- bonus: 6
  misc: null
  name: 'Deception '
- bonus: 9
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary 2
  page_start: 44
  page_stop: null
speed:
- amount: 20
  type: Land
spell_lists:
- dc: 17
  misc: null
  name: Primal Innate Spells
  spell_groups:
  - heightened_level: null
    level: 4
    spells:
    - frequency: self only
      name: dimension door
      requirement: null
  - heightened_level: null
    level: 3
    spells:
    - frequency: null
      name: mending
      requirement: null
  - heightened_level: null
    level: 1
    spells:
    - frequency: null
      name: ventriloquism
      requirement: null
  - heightened_level: 4
    level: 0
    spells:
    - frequency: null
      name: dancing lights
      requirement: null
    - frequency: null
      name: prestidigitation
      requirement: null
  to_hit: null
traits:
- N
- Tiny
- Fey
type: Creature
weaknesses:
- amount: 3
  type: <a style="text-decoration:underline" href="Equipment.aspx?ID=272">cold iron</a>
