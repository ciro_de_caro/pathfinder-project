ability_mods:
  cha_mod: 3
  con_mod: 8
  dex_mod: 4
  int_mod: -4
  str_mod: 9
  wis_mod: 6
ac: 42
ac_special: null
alignment: N
automatic_abilities: null
description: 'Aolazes are great beasts carved from stone and metal and magically imbued
  with the essence of life. The exact means of their creation is a long-lost secret,
  and they are so rare that scholars have little opportunity to study active specimens.
  The best-known aolazes are museum pieces or battlefield relics destroyed or deactivated
  centuries ago, though fragmented records suggest that many more were made and might
  remain, yet to be unearthed.




  Most aolazes are built in the shape of great land-bound beasts, such as elephants,
  rhinoceroses, or dinosaurs. Regardless of the specific creature an aolaz has been
  constructed to resemble, it is not bound to walk the earth like its inspirations
  are—it''s imbued with the magical ability to pursue across water and even through
  the air. Few can escape an aolaz''s wrath once it is earned.




  **__Recall Knowledge - Construct__ (__Arcana__, __Crafting__)**: DC 43'
hp: 255
hp_misc: null
immunities:
- bleed
- death effects
- disease
- doomed
- drained
- fatigued
- healing
- necromancy
- nonlethal attacks
- paralyzed
- poison
- sickened
- sonic
- unconscious
items: null
languages: null
level: 18
melee:
- action_cost: One Action
  damage:
    formula: 5d10+17
    type: bludgeoning
  name: trunk
  plus_damage:
  - formula: null
    type: Grab
  to_hit: 35
  traits:
  - magical
  - reach 20 feet
  - sweep
  - trip
- action_cost: One Action
  damage:
    formula: 5d8+15
    type: bludgeoning
  name: foot
  plus_damage: null
  to_hit: 33
  traits:
  - magical
  - reach 10 feet
name: Aolaz
perception: 33
proactive_abilities:
- action_cost: One Action
  critical_failure: null
  critical_success: null
  description: The aolaz tucks its head down and rolls up into an armored sphere.
    While Rolling, an aolaz has AC 44, Fort +37, Ref +29, Will +33, and Speed 100
    feet, but it can't use its trunk Strikes or its Ultrasonic Blast. It can make
    foot Strikes while rolling, but only as part of a __Trample__. The aolaz can use
    this action again to unroll and resume its standing form.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Roll
  range: null
  raw_description: '**Roll**   The aolaz tucks its head down and rolls up into an
    armored sphere. While Rolling, an aolaz has AC 44, Fort +37, Ref +29, Will +33,
    and Speed 100 feet, but it can''t use its trunk Strikes or its Ultrasonic Blast.
    It can make foot Strikes while rolling, but only as part of a __Trample__. The
    aolaz can use this action again to unroll and resume its standing form.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: Huge or smaller, foot, DC 40
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Trample
  range: null
  raw_description: '**__Trample__** [Two Actions]  Huge or smaller, foot, DC 40'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: One Action
  critical_failure: The creature takes double damage and is stunned 3.
  critical_success: The creature is unaffected.
  description: "The aolaz releases a tremendous blast of sonic energy from its trunk\
    \ in a 150-foot line, dealing 12d10 sonic damage. The frequency of this sound\
    \ is such that it is completely imperceptible to __humanoids__, but the damage\
    \ it wreaks is all too evident. Each creature in the area must attempt a DC 40\
    \ Fortitude save. The aolaz can't use Ultrasonic Blast again for 1d4 rounds. \n\
    \n"
  effect: null
  effects: null
  failure: The creature takes full damage and is stunned 2.
  frequency: null
  full_description: null
  generic_description: null
  name: Ultrasonic Blast
  range: null
  raw_description: "**Ultrasonic Blast**   (__arcane__, __evocation__, __sonic__)\
    \ The aolaz releases a tremendous blast of sonic energy from its trunk in a 150-foot\
    \ line, dealing 12d10 sonic damage. The frequency of this sound is such that it\
    \ is completely imperceptible to __humanoids__, but the damage it wreaks is all\
    \ too evident. Each creature in the area must attempt a DC 40 Fortitude save.\
    \ The aolaz can't use Ultrasonic Blast again for 1d4 rounds. \n\n**Critical Success**\
    \ The creature is unaffected. \n\n**Success** The creature takes half damage and\
    \ is __stunned 1__. \n\n**Failure** The creature takes full damage and is stunned\
    \ 2. \n\n**Critical Failure** The creature takes double damage and is stunned\
    \ 3."
  requirements: null
  success: The creature takes half damage and is stunned 1.
  traits:
  - arcane
  - evocation
  - sonic
  trigger: null
ranged: null
rarity: Rare
resistances:
- amount: 15
  type: physical (except adamantine)
ritual_lists: null
saves:
  fort: 35
  fort_misc: null
  misc: null
  ref: 27
  ref_misc: null
  will: 31
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: An aolaz has an incredible sense of hearing. It can hear any sound
    made within 1,000 feet as though it were only 5 feet away from the source of the
    sound, and any sound within 1 mile as though it were only 30 feet away from the
    source of the sound. An aolaz's hearing is a __precise sense__.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Flawless Hearing
  range: null
  raw_description: '**Flawless Hearing** An aolaz has an incredible sense of hearing.
    It can hear any sound made within 1,000 feet as though it were only 5 feet away
    from the source of the sound, and any sound within 1 mile as though it were only
    30 feet away from the source of the sound. An aolaz''s hearing is a __precise
    sense__.'
  requirements: null
  success: null
  traits: null
  trigger: null
senses:
- Perception +33
- low-light vision
- flawless hearing
size: Gargantuan
skills:
- bonus: 35
  misc: null
  name: 'Athletics '
source:
- abbr: Bestiary 2
  page_start: 21
  page_stop: null
speed:
- amount: 50
  type: Land
- amount: 6
  type: <a style="text-decoration:underline" href="Spells.aspx?ID="><i>air walk</i></a>
- amount: 371
  type: <a style="text-decoration:underline" href="Spells.aspx?ID="><i>water walk</i></a>
spell_lists:
- dc: 40
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: 9
    level: -1
    spells:
    - frequency: null
      name: air walk
      requirement: null
    - frequency: null
      name: water walk
      requirement: null
  to_hit: null
traits:
- Rare
- N
- Gargantuan
- Construct
type: Creature
weaknesses: null
