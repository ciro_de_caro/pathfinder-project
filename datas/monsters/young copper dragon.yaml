ability_mods:
  cha_mod: 3
  con_mod: 2
  dex_mod: 3
  int_mod: 2
  str_mod: 5
  wis_mod: 2
ac: 27
ac_special: null
alignment: CG
automatic_abilities:
- action_cost: None
  critical_failure: The creature is frightened 4.
  critical_success: The creature is unaffected by the presence.
  description: 90 feet, DC 23
  effect: null
  effects: null
  failure: The creature is frightened 2.
  frequency: null
  full_description: null
  generic_description: A creature that first enters the area must attempt a Will save.
    Regardless of the result of the saving throw, the creature is temporarily immune
    to this monster's Frightful Presence for 1 minute.
  name: Frightful Presence
  range: null
  raw_description: '**Frightful Presence** 90 feet, DC 23 A creature that first enters
    the area must attempt a Will save. Regardless of the result of the saving throw,
    the creature is temporarily immune to this monster''s Frightful Presence for 1
    minute.

    Critical Success The creature is unaffected by the presence.

    Success The creature is frightened 1.

    Failure The creature is frightened 2.

    Critical Success The creature is frightened 4.'
  requirements: null
  success: The creature is frightened 1.
  traits:
  - aura
  - emotion
  - fear
  - mental
  trigger: null
- action_cost: Reaction
  critical_failure: null
  critical_success: null
  description: null
  effect: The dragon makes a tail Strike at the creature with a -2 penalty. If it
    hits, the dragon disrupts the triggering action.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Twisting Tail
  range: null
  raw_description: '**Twisting Tail** [Reaction] **Trigger** A creature within reach
    of the copper dragon''s tail uses a move action or leaves a square during a move
    action it''s using. **Effect** The dragon makes a tail Strike at the creature
    with a -2 penalty. If it hits, the dragon disrupts the triggering action.'
  requirements: null
  success: null
  traits: null
  trigger: A creature within reach of the copper dragon's tail uses a move action
    or leaves a square during a move action it's using.
description: 'Capricious and always eager to share a good laugh, copper dragons are
  among the wiliest of the metallic dragons, but this by no means interferes with
  their mission to spread freedom in oppressive lands. Copper dragons are hedonists
  who are quick to indulge in simple pleasures, but they''re also sympathetic to other
  creatures, slow to pass judgment, and careful to always examine a situation from
  as many perspectives as possible. This philosophy has its drawbacks, however, as
  copper dragons are susceptible to negative influences and prone to forgiving the
  less serious evil acts performed by their chromatic cousins and other cruel creatures.
  Copper dragons also have a difficult time keeping their temper in check once they
  are roused to anger.




  Copper dragons tend to lair in warm or temperate hills, but due to their nature,
  they move their lairs every few years. They sometimes live among other people, especially
  any dwarves who reside nearby. Some copper dragons even worship gods typically worshipped
  only by humanoids. Among these dragons, worship of Cayden Cailean is most popular,
  as they see his love of freedom and penchant for alcohol aligning with the typical
  copper dragon mindset.




  A copper dragon''s combat tactics are as unorthodox as its sense of humor, as it
  is more likely to use mockery and tricks than outright strength to win its battles.
  As a copper dragon ages, it perfects jokes capable of rendering its foes helpless
  with laughter.




  **__Recall Knowledge - Dragon__ (__Arcana__)**: DC 24'
hp: 150
hp_misc: null
immunities:
- acid
- paralyzed
- sleep
items: null
languages:
- Common
- Draconic
- Gnomish
level: 8
melee:
- action_cost: One Action
  damage:
    formula: 2d8+8
    type: piercing
  name: jaws
  plus_damage:
  - formula: 1d8
    type: acid
  to_hit: 20
  traits:
  - acid
  - reach 10 feet
- action_cost: One Action
  damage:
    formula: 2d6+8
    type: slashing
  name: claw
  plus_damage: null
  to_hit: 20
  traits:
  - agile
- action_cost: One Action
  damage:
    formula: 1d8+7
    type: bludgeoning
  name: tail
  plus_damage: null
  to_hit: 18
  traits:
  - reach 15 feet
name: Young Copper Dragon
perception: 16
proactive_abilities:
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The copper dragon breathes in one of two ways. The dragon can't use
    Breath Weapon again for 1d4 rounds.
  effect: null
  effects:
  - action_cost: None
    critical_failure: null
    critical_success: null
    description: The dragon breathes acid in a 60-foot line that deals 8d6 acid damage
      (DC 26 basic Fortitude save).
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Acid
    range: null
    raw_description: '**Acid** (__acid__, __arcane__, __evocation__); The dragon breathes
      acid in a 60-foot line that deals 8d6 acid damage (DC 26 basic Fortitude save).'
    requirements: null
    success: null
    traits:
    - acid
    - arcane
    - evocation
    trigger: null
  - action_cost: None
    critical_failure: null
    critical_success: null
    description: The dragon breathes a 60-foot line of slowing gas. Each creature
      in the area must succeed at a DC 26 Fortitude save or be __slowed 1__ for 1
      round (or __slowed 2__ on a critical failure).
    effect: null
    effects: null
    failure: null
    frequency: null
    full_description: null
    generic_description: null
    name: Slowing Gas
    range: null
    raw_description: '**Slowing Gas** (__arcane__, __transmutation__); The dragon
      breathes a 60-foot line of slowing gas. Each creature in the area must succeed
      at a DC 26 Fortitude save or be __slowed 1__ for 1 round (or __slowed 2__ on
      a critical failure).'
    requirements: null
    success: null
    traits:
    - arcane
    - transmutation
    trigger: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Breath Weapon
  range: null
  raw_description: "**Breath Weapon** [Two Actions]  The copper dragon breathes in\
    \ one of two ways. The dragon can't use Breath Weapon again for 1d4 rounds. \n\
    \n  * **Acid** (__acid__, __arcane__, __evocation__); The dragon breathes acid\
    \ in a 60-foot line that deals 8d6 acid damage (DC 26 basic Fortitude save).\n\
    \n  * **Slowing Gas** (__arcane__, __transmutation__); The dragon breathes a 60-foot\
    \ line of slowing gas. Each creature in the area must succeed at a DC 26 Fortitude\
    \ save or be __slowed 1__ for 1 round (or __slowed 2__ on a critical failure)."
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: The dragon's climb speed functions only when climbing stone surfaces.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Climb Stone
  range: null
  raw_description: '**Climb Stone** The dragon''s climb speed functions only when
    climbing stone surfaces.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Two Actions
  critical_failure: null
  critical_success: null
  description: The dragon makes two claw Strikes and one tail Strike in any order.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Frenzy
  range: null
  raw_description: '**Draconic Frenzy** [Two Actions]  The dragon makes two claw Strikes
    and one tail Strike in any order.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When the dragon scores a critical hit with a Strike, it recharges Breath
    Weapon.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Draconic Momentum
  range: null
  raw_description: '**Draconic Momentum** When the dragon scores a critical hit with
    a Strike, it recharges Breath Weapon.'
  requirements: null
  success: null
  traits: null
  trigger: null
ranged: null
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 16
  fort_misc: null
  misc: +1 status to all saves vs. magic
  ref: 17
  ref_misc: null
  will: 16
  will_misc: null
sense_abilities: null
senses:
- Perception +16
- darkvision
- scent (imprecise) 60 feet
size: Large
skills:
- bonus: 17
  misc: null
  name: 'Acrobatics '
- bonus: 17
  misc: null
  name: 'Athletics '
- bonus: 14
  misc: null
  name: 'Crafting '
- bonus: 15
  misc: null
  name: 'Deception '
- bonus: 16
  misc: null
  name: 'Performance '
- bonus: 16
  misc: null
  name: 'Society '
- bonus: 17
  misc: null
  name: 'Stealth '
source:
- abbr: Bestiary
  page_start: 121
  page_stop: null
speed:
- amount: 30
  type: Land
- amount: 120
  type: fly
- amount: 30
  type: climb stone
spell_lists:
- dc: 27
  misc: null
  name: Arcane Innate Spells
  spell_groups:
  - heightened_level: null
    level: 1
    spells:
    - frequency: at will
      name: grease
      requirement: null
  to_hit: null
traits:
- CG
- Large
- Dragon
- Earth
type: Creature
weaknesses: null
