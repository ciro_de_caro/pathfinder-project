ability_mods:
  cha_mod: 5
  con_mod: 1
  dex_mod: 4
  int_mod: 1
  str_mod: 3
  wis_mod: 2
ac: 19
ac_special: null
alignment: CN
automatic_abilities: null
description: 'To a satyr, life is a party and everyone is invited. Notorious for their
  hedonism, these fey believe there''s no greater beauty than can be found in song,
  drink, indulgent meals, and carnal pleasures. Satyrs use their enchanting songs
  and natural charm to encourage all manner of people to follow their true desires
  and free themselves from society''s rules. This usually involves enticing mortals
  to join raucous parties or engage in trysts in moonlit glades. If a potential companion
  rejects a satyr''s advances, however, the satyr has little interest in continuing
  a conversation and goes off to find more amenable revelers.




  The lifestyle of a satyr leaves no room for ongoing affairs or long-term friends.
  Once his party is over or his lust is satiated, the satyr disappears back into the
  forest. The offspring satyrs leave behind are satyrs themselves, and usually end
  up being taken from their cradles by other fey rather than being left in mortals''
  care. Satyrs are always male.




  The untouched beauty of the forest is sacred and precious to a satyr. Brutish intruders
  who clear-cut trees or massacre animals without eating them risk drawing a satyr''s
  ire. A satyr so provoked uses his spells to undermine foes and attempts to dispatch
  them either with brutal ambushes or by leading a rush of forest animals to attack.




  Other fey, particularly good fey, look upon satyrs as loutish, embarrassing cousins.
  They''re rarely hostile toward satyrs, but most find them insufferable and advise
  any mortals they like to steer clear of satyrs'' glades.




  **__Recall Knowledge - Fey__ (__Nature__)**: DC 19'
hp: 80
hp_misc: null
immunities: null
items:
- dagger
- panpipes
- shortbow (20 arrows)
- wineskin
languages:
- Common
- Sylvan
level: 4
melee:
- action_cost: One Action
  damage:
    formula: 1d4+6
    type: piercing
  name: dagger
  plus_damage: null
  to_hit: 14
  traits:
  - agile
  - finesse
  - versatile S
name: Satyr
perception: 10
proactive_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: When the satyr Plays the Pipes to cast a spell, he can Step or Stride
    as part of the activity.
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Fleet Performer
  range: null
  raw_description: '**Fleet Performer** When the satyr Plays the Pipes to cast a spell,
    he can Step or Stride as part of the activity.'
  requirements: null
  success: null
  traits: null
  trigger: null
- action_cost: Three Actions
  critical_failure: null
  critical_success: null
  description: null
  effect: The satyr is holding a musical instrument.
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Play the Pipes
  range: null
  raw_description: '**Play the Pipes** [Three Actions]  (__auditory__, __primal__)
    **Requirements** The satyr is holding a musical instrument. **Effect** The satyr
    plays a melody on his instrument to cast __charm__, __fear__, __sleep__, or __suggestion__
    without expending the spell slot and using his music in place of providing the
    spell''s component actions. The spell gains the __auditory__ trait and targets
    all creatures in a 60-foot emanation instead of its usual targets. A creature
    that succeeds at its Will save against any spell is then temporarily immune from
    spells played from that satyr''s pipes for 1 minute. Satyrs are immune to this
    music.'
  requirements: null
  success: null
  traits:
  - auditory
  - primal
  trigger: null
ranged:
- action_cost: One Action
  damage:
    formula: 1d6+3
    type: piercing
  name: shortbow
  plus_damage: null
  to_hit: 14
  traits:
  - deadly 1d10
  - range increment 60 feet
  - reload 0
- action_cost: One Action
  damage:
    formula: 1d4+6
    type: piercing
  name: dagger
  plus_damage: null
  to_hit: 14
  traits:
  - agile
  - thrown 10 feet
  - versatile S
rarity: Common
resistances: null
ritual_lists: null
saves:
  fort: 9
  fort_misc: null
  misc: null
  ref: 11
  ref_misc: null
  will: 12
  will_misc: null
sense_abilities:
- action_cost: None
  critical_failure: null
  critical_success: null
  description: 'A satyr''s wineskin magically enchants any alcohol inside. With an
    Interact action, a living creature can imbibe the alcohol and gain a +1 item bonus
    to Will saves and a +3 item bonus to Will saves against fear effects for the following
    hour. When the wineskin is removed from a satyr''s person, the magic remains only
    until the wine spoils. The wineskin holds up to eight drafts of wine.


    '
  effect: null
  effects: null
  failure: null
  frequency: null
  full_description: null
  generic_description: null
  name: Sylvan Wine
  range: null
  raw_description: '**Sylvan Wine** (__enchantment__, __mental__, __primal__) A satyr''s
    wineskin magically enchants any alcohol inside. With an Interact action, a living
    creature can imbibe the alcohol and gain a +1 item bonus to Will saves and a +3
    item bonus to Will saves against fear effects for the following hour. When the
    wineskin is removed from a satyr''s person, the magic remains only until the wine
    spoils. The wineskin holds up to eight drafts of wine.


    '
  requirements: null
  success: null
  traits:
  - enchantment
  - mental
  - primal
  trigger: null
senses:
- Perception +10
- low-light vision
size: Medium
skills:
- bonus: 8
  misc: null
  name: 'Athletics '
- bonus: 13
  misc: null
  name: 'Deception '
- bonus: 13
  misc: null
  name: 'Diplomacy '
- bonus: 11
  misc: null
  name: 'Intimidation '
- bonus: 9
  misc: null
  name: 'Nature '
- bonus: 13
  misc: null
  name: 'Performance '
- bonus: 11
  misc: null
  name: 'Stealth '
- bonus: 8
  misc: null
  name: 'Survival '
source:
- abbr: Bestiary
  page_start: 284
  page_stop: null
speed:
- amount: 35
  type: Land
spell_lists:
- dc: 21
  misc: null
  name: Primal Innate Spells
  spell_groups:
  - heightened_level: null
    level: 4
    spells:
    - frequency: null
      name: charm
      requirement: null
    - frequency: null
      name: fear
      requirement: null
    - frequency: null
      name: sleep
      requirement: null
    - frequency: null
      name: suggestion
      requirement: null
  - heightened_level: 2
    level: 0
    spells:
    - frequency: null
      name: dancing lights
      requirement: null
    - frequency: null
      name: ghost sound
      requirement: null
    - frequency: null
      name: inspire competence
      requirement: null
    - frequency: null
      name: inspire courage
      requirement: null
    - frequency: null
      name: triple time
      requirement: null
  to_hit: null
traits:
- CN
- Medium
- Fey
type: Creature
weaknesses:
- amount: 5
  type: cold iron
